from proyecto_login import db, gestor
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin


@gestor.user_loader
def load_user(Usuario_id):
    return Usuario.query.get(Usuario_id)


class Usuario(db.Model, UserMixin):
    __tablename__ = 'usuarios'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    nombre = db.Column(db.String(64), unique=True, index=True)
    passw_encriptada = db.Column(db.String(128))

    def __init__(self, nombre, passw, email):
        self.nombre = nombre
        self.email = email
        self.passw_encriptada = generate_password_hash(passw)

    def verificar_passw(self, passw):
        return check_password_hash(self.passw_encriptada, passw)
