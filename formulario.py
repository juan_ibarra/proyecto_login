import email_validator
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Email, EqualTo
from wtforms import ValidationError
from proyecto_login.modelos import Usuario


class FormularioRegistro(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    nombre = StringField('Nombre', validators=[DataRequired()])
    passw = PasswordField('Password', validators=[DataRequired(), EqualTo('passw_repetir', message='Las password no coninciden')])
    passw_repetir = PasswordField('Repetir la Password', validators=[DataRequired()])
    boton = SubmitField('Registrar')

    def verificar_mail(self, parametro):
        if Usuario.query.filter_by(email=parametro.data).first():
            raise ValidationError('Error. Este mail ya ha sido utilizado')

    def verificar_nombre(self, parametro):
        if Usuario.query.filter_by(nombre=parametro.data).first():
            raise ValidationError('Error. Este nombre ya ha sido utilizado')


class FormularioLogin(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    passw = PasswordField('Password', validators=[DataRequired()])
    boton = SubmitField('Entrar')
